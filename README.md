# Formation UI

# Environnement Superprod

- PySide2-5.15.2
- qt-5.12.6

## Objectif principal : créer ses propres interfaces dans Maya

### Créer un script python en dehors de maya

| Operating System | commands                 |
| ---------------- | ------------------------ |
| Linux            | `touch FILENAME.py`      |
| Windows          | `type nul > FILENAME.py` |
