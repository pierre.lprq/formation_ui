# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'interface_01.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if not MainWindow.objectName():
            MainWindow.setObjectName("MainWindow")
        MainWindow.resize(688, 459)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.widget = QWidget(self.centralwidget)
        self.widget.setObjectName("widget")
        self.widget.setAutoFillBackground(False)
        self.widget.setStyleSheet("")
        self.verticalLayout_2 = QVBoxLayout(self.widget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalSpacer_2 = QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding
        )

        self.verticalLayout_2.addItem(self.verticalSpacer_2)

        self.my_label = QLabel(self.widget)
        self.my_label.setObjectName("my_label")
        font = QFont()
        font.setPointSize(25)
        self.my_label.setFont(font)
        self.my_label.setAlignment(Qt.AlignCenter)

        self.verticalLayout_2.addWidget(self.my_label)

        self.verticalSpacer = QSpacerItem(
            20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding
        )

        self.verticalLayout_2.addItem(self.verticalSpacer)

        self.verticalLayout.addWidget(self.widget)

        self.widget_2 = QWidget(self.centralwidget)
        self.widget_2.setObjectName("widget_2")
        self.horizontalLayout = QHBoxLayout(self.widget_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.my_checker = QCheckBox(self.widget_2)
        self.my_checker.setObjectName("my_checker")

        self.horizontalLayout.addWidget(self.my_checker)

        self.horizontalSpacer = QSpacerItem(
            40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum
        )

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.my_display = QPushButton(self.widget_2)
        self.my_display.setObjectName("my_display")
        self.my_display.setStyleSheet("background-color: rgb(145, 220, 84);")

        self.horizontalLayout.addWidget(self.my_display)

        self.my_erase = QPushButton(self.widget_2)
        self.my_erase.setObjectName("my_erase")
        self.my_erase.setStyleSheet("background-color: rgb(255, 83, 83);")

        self.horizontalLayout.addWidget(self.my_erase)

        self.verticalLayout.addWidget(self.widget_2)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName("menubar")
        self.menubar.setGeometry(QRect(0, 0, 688, 21))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.my_checker.toggled.connect(self.my_display.setDisabled)

        QMetaObject.connectSlotsByName(MainWindow)

    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(
            QCoreApplication.translate("MainWindow", "MainWindow", None)
        )
        self.my_label.setText("")
        self.my_checker.setText(
            QCoreApplication.translate("MainWindow", "CheckBox", None)
        )
        self.my_display.setText(
            QCoreApplication.translate("MainWindow", "Display", None)
        )
        self.my_erase.setText(QCoreApplication.translate("MainWindow", "Erase", None))

    # retranslateUi
