# coding: utf-8

from PySide2 import QtWidgets
from functools import partial
import sys


class MyWindow(QtWidgets.QMainWindow):
    """Create Qt window that inherits from QMainWindow."""

    def __init__(self, parent=None):
        """Init function."""

        # Call QMainWindow's __init__ method
        super(MyWindow, self).__init__(parent=parent)

        # Define window settings
        self.setWindowTitle("My Window")
        self.setGeometry(200, 200, 400, 300)

        # Our method to define the user interface
        self.init_ui()

    def init_ui(self):
        """UI init function."""

        # Define window's default layout
        # https://doc.qt.io/qtforpython/overviews/layout.html
        self.central_widget = QtWidgets.QWidget(self)
        self.default_layout = QtWidgets.QVBoxLayout(self.central_widget)

        # Create Label
        self.my_label = QtWidgets.QLabel(self.central_widget)
        self.my_label.setText("My Label")

        # Create Button - Logs
        self.my_button_1 = QtWidgets.QPushButton(self.central_widget)
        self.my_button_1.setText("Display Logs")
        self.my_button_1.clicked.connect(partial(self.log, "pressed"))

        # Create Button - Update UI
        self.my_button_2 = QtWidgets.QPushButton(self.central_widget)
        self.my_button_2.setText("Update UI")
        self.my_button_2.clicked.connect(partial(self.my_label.setText, "Hello World"))

        # Add to Layout
        self.default_layout.addWidget(self.my_label)
        self.default_layout.addWidget(self.my_button_1)
        self.default_layout.addWidget(self.my_button_2)
        self.setCentralWidget(self.central_widget)

    def log(self, log_message):
        """logger."""
        print(log_message)


def create_application():
    """Launch Qt application."""

    # Create new Qt application
    app = QtWidgets.QApplication(sys.argv)

    # Create object MyWindow
    my_window = MyWindow()

    # Show my_window
    my_window.show()

    # Used to end application properly
    sys.exit(app.exec_())


if __name__ == "__main__":
    create_application()
