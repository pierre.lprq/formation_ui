# coding: utf-8
"""Module 01-02 : Create simple Qt class."""

from PySide2 import QtWidgets
import sys


class MyWindow(QtWidgets.QMainWindow):
    """Create Qt window that inherits from QMainWindow."""

    def __init__(self, parent=None):
        """Init function."""

        # Call QMainWindow __init__ method
        super(MyWindow, self).__init__(parent=parent)

        # In Maya, if your class inherit from QWidget,
        # it must be flaged as `window` to be considered as such
        # self.setWindowFlags(QtCore.Qt.Window)

        # Define window settings
        self.setWindowTitle("My Window")
        self.setGeometry(200, 200, 400, 300)

        # Our method to define the user interface
        self.init_ui()

    def init_ui(self):
        """UI init function."""

        # Define window's default layout
        # https://doc.qt.io/qtforpython/overviews/layout.html
        self.central_widget = QtWidgets.QWidget(self)
        self.default_layout = QtWidgets.QVBoxLayout(self.central_widget)

        # Create a widget that will contain our labels
        self.my_list = []
        self.my_container = QtWidgets.QWidget(self.central_widget)
        self.container_layout = QtWidgets.QVBoxLayout(self.my_container)

        # Create Button
        self.my_button = QtWidgets.QPushButton(self.central_widget)
        self.my_button.setText("My Button")
        self.my_button.clicked.connect(self.add_label)

        # Add to Layout
        self.default_layout.addWidget(self.my_container)
        self.default_layout.addWidget(self.my_button)
        self.setCentralWidget(self.central_widget)

    def add_label(self):
        """Add Label to my_container."""
        new_label = QtWidgets.QLabel(self.my_container)
        new_label.setText(str(len(self.my_list)))
        self.container_layout.addWidget(new_label)
        self.my_list.append(new_label)

    def log(self, log_message):
        """logger."""
        print(log_message)


def create_application():
    """Launch Qt application."""

    # Create new Qt application
    app = QtWidgets.QApplication(sys.argv)

    # Create object MyWindow
    my_window = MyWindow()

    # Show my_window
    my_window.show()

    # Used to end application properly
    sys.exit(app.exec_())


if __name__ == "__main__":
    create_application()
