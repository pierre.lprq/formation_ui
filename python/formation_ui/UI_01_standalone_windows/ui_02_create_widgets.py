# coding: utf-8
"""Module 01-01 : Create simple Qt window."""

# PySide2 : used to import Qt framework
# source : https://srinikom.github.io/pyside-docs/contents.html
# source : https://wiki.qt.io/Qt_for_Python
# source : https://doc.qt.io/qtforpython-5/index.html
# source : https://doc.qt.io/qtforpython/
from PySide2 import QtWidgets

# partial : used to create a partial object that runs the given function when it's called
# https://docs.python.org/3.9/library/functools.html
from functools import partial

# sys : used to get system parameters
# source : https://docs.python.org/3/library/sys.html
import sys


def debug(text="text", suite="suite"):
    """Print the given arguments in the console."""
    print(text, suite)


def create_window():
    """Create Qt window with some widgets."""

    # Create new application on system
    app = QtWidgets.QApplication(sys.argv)

    # Define the window's type
    win = QtWidgets.QMainWindow()

    # Define window settings
    win.setWindowTitle("My Window")
    win.setGeometry(200, 200, 400, 300)

    # ------------------ Adding some widgets ------------------ #

    # Create Label
    # https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QLabel.html
    label = QtWidgets.QLabel(win)
    label.setText("my label")
    label.move(100, 100)  # x_pos, y_pos

    # Create Button - Logs
    # https://doc.qt.io/qtforpython-5/PySide2/QtWidgets/QPushButton.html
    button_1 = QtWidgets.QPushButton(win)
    button_1.setText("Display logs")
    button_1.move(100, 150)  # x_pos, y_pos

    # Create Button - Update UI
    button_2 = QtWidgets.QPushButton(win)
    button_2.setText("Update UI")
    button_2.move(100, 200)  # x_pos, y_pos

    # We can connect button's click events to a function
    button_1.clicked.connect(partial(debug, suite="1", text="2"))

    # We can also link these events to update an ui element
    button_2.clicked.connect(partial(label.setText, "Hello World"))

    # But it's becoming more difficult to edit an existing ui elements
    # of our current window if we call an external functions :
    # - we would have to give the ui element as argument to each function

    # ------------------ End ------------------ #

    # Show window
    win.show()

    # Used to end an app properly
    sys.exit(app.exec_())


if __name__ == "__main__":
    create_window()
