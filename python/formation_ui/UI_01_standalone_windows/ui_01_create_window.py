# coding: utf-8
"""Module 01-01 : Create simple Qt window."""

# PySide2 : used to import Qt framework
# source : https://srinikom.github.io/pyside-docs/contents.html
# source : https://wiki.qt.io/Qt_for_Python
# source : https://doc.qt.io/qtforpython-5/index.html
# source : https://doc.qt.io/qtforpython/
from PySide2 import QtWidgets

# sys : used to get system parameters
# source : https://docs.python.org/3/library/sys.html
import sys


def create_window():
    """Create Qt window."""

    # QApplication: create new application on system
    # sys.argv : all arguments given through the command line
    app = QtWidgets.QApplication(sys.argv)

    # Define the window's type
    win = QtWidgets.QMainWindow()

    # Define window's title
    win.setWindowTitle("My Window")

    # Define window's position and size
    win.setGeometry(200, 200, 400, 300)  # x_pos, y_pos, width, height

    # Show window
    win.show()

    # Used to end an app properly
    # app.exec_()     : used to launch the QEventLoop. It also returns the error code when QApplication crash
    # sys.exit(error) : used to quit current python script execution and raise the given error if any
    return_code = app.exec_()
    sys.exit(return_code)


if __name__ == "__main__":
    create_window()
