# coding: utf-8

# import Qt : import Qt framework
# source : https://doc.qt.io/qtforpython-5/index.html
from PySide2 import QtWidgets, QtCore

# import Qt : shiboken2 - used to access Qt internal informations
# source : https://doc.qt.io/qtforpython/shiboken6/shibokenmodule.html
from shiboken2 import wrapInstance

# import maya : OpenMayaUI - used to get QWidget pointer to Maya's main window
# source : https://help.autodesk.com/view/MAYAUL/2020/ENU/?guid=Maya_SDK_MERGED_cpp_ref_class_m_qt_util_html
import maya.OpenMayaUI as mui


class maya_window(QtWidgets.QMainWindow):
    """Create Qt window with a class."""

    def __init__(self, parent=None):
        # Call QMainWindow __init__ method
        super(maya_window, self).__init__(parent=parent)

        # Define window settings
        self.setWindowFlags(
            self.windowFlags() | QtCore.Qt.Window | QtCore.Qt.WindowStaysOnTopHint
        )
        self.setWindowTitle("My Window")
        self.setGeometry(200, 200, 400, 300)


def OpenMaya_launcher():
    """Launch Qt window by using a class."""
    # Get pointer / memory adress to current maya application
    ptr = mui.MQtUtil.mainWindow()
    # Get python Qt object from pointer
    parent = wrapInstance(long(ptr), QtWidgets.QWidget)
    # Initialise maya_window
    my_window = maya_window(parent)
    # Show maya_window
    my_window.show()


def custom_launcher():
    """Launch Qt window by using a function."""
    # Create global variable to keep window's instance in memory
    global MAYA_WINDOW
    # Initialise maya_window
    if "MAYA_WINDOW" not in globals():
        MAYA_WINDOW = maya_window()
    # Show maya_window
    MAYA_WINDOW.show()
