from PySide2 import QtCore, QtQuick, QtGui

import os
import sys


def create_application():
    """Launch Qt application."""

    # Create new Qt application
    app = QtGui.QGuiApplication(sys.argv)

    import main

    # Create object MyWindow
    my_window = main.MyWindow(edit=True)

    # Show my_window
    my_window.show()

    # Used to end application properly
    sys.exit(app.exec_())


if __name__ == "__main__":
    create_application()
