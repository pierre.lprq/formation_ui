import QtQuick 2.0
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Item {
    id: root
    anchors.fill: parent

    ColumnLayout {
        anchors.fill: parent

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true

            color: "blue"
        }

        Label {
            Layout.fillWidth: true
            Layout.topMargin: 5
            Layout.leftMargin: 15
            Layout.rightMargin: 10

            color: "green"
            text: qsTr("Assigned To")
        }
    }

}