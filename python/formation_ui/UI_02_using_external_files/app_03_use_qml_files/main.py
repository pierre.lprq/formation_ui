# coding: utf-8
"""Module 01-02 : Create simple Qt class."""

from PySide2 import QtQuick, QtCore
import os


class MyWindow(QtQuick.QQuickView):
    """Create Qt window that inherits from QMainWindow."""

    def __init__(self, parent=None, edit=False):
        """Init function."""

        # Call QMainWindow __init__ method
        super(MyWindow, self).__init__(parent=parent)

        self.init_ui(edit=edit)

    def init_ui(self, edit=False):
        """UI init function."""
        # Create engine to link functions to QML
        self.engine = self.engine()
        self.context = self.engine.rootContext()
        self.context.setContextProperty("context", self)

        # Load the QML file
        project_path = os.path.dirname(os.path.abspath(__file__))
        qml_file_path = os.path.join(project_path, "ui", "qml", "main.qml")
        self.setSource(QtCore.QUrl.fromLocalFile(qml_file_path))

        if edit:
            # Watcher
            qml_path = os.path.join(project_path, "ui", "qml")
            ui_files = [os.path.join(qml_path, x)
                        for x in os.listdir(qml_path)]
            self.watcher = QtCore.QFileSystemWatcher(ui_files)
            self.watcher.fileChanged.connect(self.update_ui)

    def update_ui(self, path):
        self.engine.clearComponentCache()
        project_path = os.path.dirname(os.path.abspath(__file__))
        qml_file_path = os.path.join(project_path, "ui", "qml", "main.qml")
        self.setSource(QtCore.QUrl.fromLocalFile(
            qml_file_path
        ))

    @QtCore.Slot(result=str)
    def say_hello(self):
        return "Hello World"
