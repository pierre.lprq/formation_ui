from PySide2 import QtWidgets
import sys


def create_application(edit=False):
    """Launch Qt application."""

    # Create new Qt application
    app = QtWidgets.QApplication(sys.argv)

    import main

    # Create object MyWindow
    my_window = main.MyWindow()

    # Show my_window
    my_window.show()

    # Used to end application properly
    sys.exit(app.exec_())


if __name__ == "__main__":
    create_application(edit=True)
