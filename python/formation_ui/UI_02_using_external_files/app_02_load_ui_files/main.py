# coding: utf-8
"""Module 01-02 : Create simple Qt class."""

from PySide2 import QtWidgets, QtCore
from PySide2 import QtUiTools
import os


class MyWindow(QtWidgets.QMainWindow):
    """Create Qt window that inherits from QMainWindow."""

    def __init__(self, parent=None):
        """Init function."""

        # Call QMainWindow __init__ method
        super(MyWindow, self).__init__(parent=parent)

        self.setWindowFlags(QtCore.Qt.Window)

        # Define window settings
        self.setWindowTitle("My Window")
        self.setGeometry(200, 200, 400, 300)

        # Our method to load the user interface
        self.load_ui()

    def load_ui(self):
        """UI init function."""
        # Get ui file path
        ui_file = os.path.join(
            os.path.split(__file__)[0], "ui_files", "interface_01.ui"
        )
        ui_file = os.path.normpath(ui_file).replace(os.sep, "/")

        ui_file = QtCore.QFile(ui_file)
        self.ui = QtUiTools.QUiLoader().load(ui_file)

        self.ui.my_display.clicked.connect(self.say_hello)
        self.ui.my_erase.clicked.connect(self.clean)

    def show(self):
        """Redefine show function."""
        self.ui.show()

    def say_hello(self):
        """Say hello."""
        self.ui.my_label.setText("Hello World")

    def clean(self):
        """Say nothing."""
        self.ui.my_label.setText("")

    def log(self, log_message):
        """logger."""
        print(log_message)
