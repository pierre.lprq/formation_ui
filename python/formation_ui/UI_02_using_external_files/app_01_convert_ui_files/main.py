# coding: utf-8
"""Module 01-02 : Create simple Qt class."""

from PyQt5 import QtWidgets, QtCore

from ui_files import interface_01


class MyWindow(QtWidgets.QMainWindow, interface_01.Ui_MainWindow):
    """Create Qt window that inherits from QMainWindow."""

    def __init__(self, parent=None):
        """Init function."""

        # Call QMainWindow __init__ method
        super(MyWindow, self).__init__(parent=parent)

        self.setWindowFlags(QtCore.Qt.Window)

        # Define window settings
        self.setWindowTitle("My Window")
        self.setGeometry(200, 200, 400, 300)

        # Our method to define the user interface
        self.init_ui()

    def init_ui(self):
        """UI init function."""
        self.setupUi(self)

        self.my_display.clicked.connect(self.say_hello)
        self.my_erase.clicked.connect(self.clean)

    def say_hello(self):
        """Say hello."""
        self.my_label.setText("Hello World")

    def clean(self):
        """Say nothing."""
        self.my_label.setText("")

    def log(self, log_message):
        """logger."""
        print(log_message)
