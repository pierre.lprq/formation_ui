from PyQt5 import QtWidgets
import os
import sys


def create_application(edit=False):
    """Launch Qt application."""

    # Create new Qt application
    app = QtWidgets.QApplication(sys.argv)

    if edit:
        convert_ui_files()
        print("converted")

    import main

    # Create object MyWindow
    my_window = main.MyWindow()

    # Show my_window
    my_window.show()

    # Used to end application properly
    sys.exit(app.exec_())


def convert_ui_files():
    # https://www.riverbankcomputing.com/static/Docs/PyQt5/index.html
    from PyQt5 import uic

    # Get ui files directory
    dir_path = os.path.join(os.path.split(__file__)[0], "ui_files")
    # Clean path
    dir_path = os.path.normpath(dir_path).replace(os.sep, "/")
    # Convert ui files
    uic.compileUiDir(dir_path)

    # With PySide2 :
    # import pyside2uic
    # pyside2uic.compileUiDir(dir_path)


if __name__ == "__main__":
    create_application(edit=True)
